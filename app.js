const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/flights', (req, res) => {
  const registration = req.query.registration
  if (registration) {
    res.send(`Flights for ${registration.toUpperCase()}`)
  } else {
    res.send(`No registration provided`)
  }
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))